

var cas = {};

var usefulData = [];
var responseData;
var paramsOfInterest = ['title','channelTitle','channelId','publishedAt',
                        'viewCount','likeCount','commentCount','dislikeCount',
                       'duration','definition'];
var avalParams = ['title','channelTitle','channelId','publishedAt',
                        'viewCount','likeCount','commentCount','dislikeCount',
                       'duration','definition'];
var prefix = {
              'definition':     'contentDetails', 
              'duration':       'contentDetails', 
              'publishedAt':    'snippet',
              'title':          'snippet',
              'channelTitle':   'snippet',
              'channelId':      'snippet',
              'commentCount':   'statistics',
              'dislikeCount':   'statistics',
              'likeCount':      'statistics',
              'viewCount':      'statistics'
             }
var sortedData = [];

var transform = {"<>":"li","html":[
                    {"<>":"span","id":"${title}","html":"${title}: ${data}"}
                ]};
var videoTransform = {"<>":"div","class":"embed-responsive embed-responsive-16by9","html":[
                        {"<>":"iframe","src":"https://www.youtube.com/embed/${data}","html":""}
                    ]};

// Called automatically when JavaScript client library is loaded.
function onClientLoad() {
    gapi.client.load('youtube', 'v3', onYouTubeApiLoad);
}

// Called automatically when YouTube API interface is loaded (see line 9).
function onYouTubeApiLoad() {
    gapi.client.setApiKey('AIzaSyCzYP_jPzEEKiOX5vQ2f9hj_pFk2fVqhZA');
    // This is just for testing purposes to speed up things.
    search('canada', 10);
}

// called when search is performed
function search(query, resNr) {        
    // Use the JavaScript client library to create a search.list() API call.
    usefulData = [];
    responseData = [];
    sortedData = [];
    var request = gapi.client.youtube.search.list({
        part: 'snippet',
        q: query,
        type: "video",
        maxResults: resNr
    });
    
    // Send the request to the API server,
    // and invoke onSearchRepsonse() with the response.
    request.execute(onSearchResponse);
}

function parseResponse(response){
    var data = response;
    
    document.getElementById('responseHead').innerHTML = JSON.stringify(response.pageInfo);    
    
    // helper log for confirnming the structure of data
    console.log(data.items[0]);
    
    for(itemNr in data.items){
        
        var url="https://www.youtube.com/embed/"+data.items[itemNr].id;                
        var videoItem = {
            'head': {'url': url, 'title' : data.items[itemNr].snippet.title },
            'metadata' : []            
        };
        for(paramNr in avalParams){
            var tmp = "data.items[itemNr]."+prefix[(paramsOfInterest[paramNr])]+"."+(paramsOfInterest[paramNr]);
            videoItem.metadata.push({'title': paramsOfInterest[paramNr], 'data': eval(tmp)});
        }
        usefulData.push(videoItem);                
//        document.getElementById("response").innerHTML+=JSON.stringify(videoItem)+"<br><br>";
    }
    showData("response", usefulData);
}

function rerankResponse(response){
    var data = response;
        
    // helper log for confirnming the structure of data
    console.log(data.items[0]);    
    
    document.getElementById("sortedResponse").innerHTML="";
    for(paramNr in paramsOfInterest){
     sortedData[paramNr]=[] ;
    }   
    for(itemNr in data.items){
        
        var url="https://www.youtube.com/embed/"+data.items[itemNr].id;                
        var videoItem = {
            'head': {'url': url, 'title' : data.items[itemNr].snippet.title },
            'metadata' : []            
        };        
        for(paramNr in paramsOfInterest){
            var tmp = "data.items[itemNr]."+prefix[(paramsOfInterest[paramNr].checked)]+"."+(paramsOfInterest[paramNr].checked);
            var tmpVal = paramsOfInterest[paramNr].value;
            if( !parseInt(tmpVal) )
              tmpVal = 0;
            //console.log(paramsOfInterest[paramNr]);
          
            var tmpDistance = 0;
            switch(paramsOfInterest[paramNr].checked){
              case 'viewCount':
              case 'likeCount':
              case 'commentCount':
              case 'dislikeCount':
                tmpDistance = (Math.abs(eval(tmp)-tmpVal));
                break;
              case 'duration':
                  tmpDistance = moment.duration( eval(tmp) ).asMilliseconds()
                    -moment.duration( tmpVal ).asMilliseconds();
                  break;
              case 'publishedAt':
                  var aa = moment(eval(tmp));
                  console.log(aa);
                  var bb = moment(tmpVal);
                  console.log(bb);
                   // 86400000
                  console.log(aa.diff(bb));
                  tmpDistance = aa.diff(bb, 'seconds');
                  break;
              default:
                  tmpDistance = 0;                
            }
            
          
          
            var tmpWeight = paramsOfInterest[paramNr].weight;
            videoItem.metadata.push({'title': paramsOfInterest[paramNr].checked, 
                                     'data': eval(tmp), 
                                     'distanceFromExample': Math.abs(tmpDistance)
                                    });   
            videoItem.order = 0;
        }
        for(paramNr in paramsOfInterest){
         sortedData[paramNr].push(videoItem);
        }       
    }
//    console.log(sortedData);
    // sorts the data for each choosen parameter
    for(paramNr in paramsOfInterest){
      sortedData[paramNr].sort(function(a, b){
        return a.metadata[paramNr].distanceFromExample-b.metadata[paramNr].distanceFromExample;
//        switch(a.metadata[paramNr].title){
//          case 'viewCount':
//          case 'likeCount':
//          case 'commentCount':
//          case 'dislikeCount':
//              return a.metadata[paramNr].data-b.metadata[paramNr].data;
//              break;
//          case 'duration':
//              return moment.duration( a.metadata[paramNr].distanceFromExample ).asMilliseconds()
//                -moment.duration( b.metadata[paramNr].distanceFromExample ).asMilliseconds();
//              break;
//          case 'publishedAt':
//              var aa = moment(a.metadata[paramNr].data);
//              console.log(aa);
//              var bb = moment(b.metadata[paramNr].data);
//              console.log(bb);
//               // 86400000
//              console.log(aa.diff(bb));
//              return aa.diff(bb, 'seconds');
//              break;
//          default:
//              return 0;
//            
////distance from Google Maps API that would be used to rerank by geolocation
////computeDistanceBetween(from:LatLng, to:LatLng, radius?:number)  
//// Returns the distance, in meters, between two LatLngs. You can optionally specify a custom radius. The radius defaults to the radius of the Earth.            
//            
//        }
        
      });
    }
//    console.log("====");
  
    // for all parameters that have been used for reranking this adds up the rank by that parameter and gives the final rank.
    for(paramNr in paramsOfInterest){
      //console.log(sortedData[paramNr].length);
      for(var it = 0; it < sortedData[paramNr].length; it++){
        sortedData[paramNr][it].order += (it*paramsOfInterest[paramNr].weight);
      }      
    }
//    console.log("====");
//    console.log(sortedData);   
    if(sortedData.length > 0){
      // sorts the data by the final rank given from adding up the partial ranks
      sortedData[0].sort(function(a, b){
        return a.order-b.order;
      });  
      cas.end = moment();
      showData("sortedResponse", sortedData[0]);      
    } else {
      cas.end = moment();      
      showData("sortedResponse", [{'head':{'url':'#','title':'No parameters'},'metadata':{} }]);
    }
    document.getElementById('rerankHead').innerHTML = "Time to complete the sorting: "+cas.end.diff(cas.begin)+" miliseconds.";
    
}

function parseDuration(dur){
    console.log(dur);
    console.log(moment.duration(dur).asMilliseconds());
    return 0;
}

function showData(elementId, data){
    console.log('show data');
    console.log(data);
    document.getElementById(elementId).innerHTML = "";
    for(iter in data){
      var itemHead = '<h3 class="panel-title"><a href="'+data[iter].head.url+'">'+data[iter].head.title+'</a></h3>';
      var itemData = '<div>'+
          //JSON.stringify(data[iter].metadata)+
          json2html.transform(data[iter].metadata,transform)+
          '</div>';
      document.getElementById(elementId).innerHTML += '<div class="panel panel-default">'+
        '<div class="panel-heading">'+itemHead+'</div>'+
        '<div class="panel-body">'+itemData+'</div>'+
        '</div>';
      
    }
}

function getRerankParams(){
    cas.begin = moment();
    console.log("Rerank response");
    var checkboxes = document.getElementsByName('paramOfInterest');
    document.getElementById('sortedResponse').innerHTML = "";
    //console.log(checkboxes);
    paramsOfInterest = [];
    for(chNr=0; chNr < checkboxes.length; chNr+=3){
        if(checkboxes[chNr].checked){
//           console.log(  
//                        JSON.stringify({ 
//                            'checked': checkboxes[chNr].value, 
//                            'value' : checkboxes[chNr+1].value,
//                            'weight' : checkboxes[chNr+2].value
//                        })
//                        );
           paramsOfInterest.push({ 
                                  'checked': checkboxes[chNr].value, 
                                  'value' : checkboxes[chNr+1].value,
                                  'weight' : checkboxes[chNr+2].value
                                });    
        }          
    }    
    rerankResponse(responseData);        
}


// Called automatically with the response of the YouTube API request.
function onSearchResponse(response) {
    if ('error' in response) {
        console.log(response.error.message);
      } else {
        if ('items' in response) {
          var videoIds = [];
          for(i in response.items){
              videoIds.push(response.items[i].id.videoId);
          }    
          // Now that we know the IDs of all the videos in the uploads list,
          // we can retrieve information about each video.
          getVideoMetadata(videoIds);
        } else {
          console.log('There are no videos found.');
        }
      }
}


// get the metadata of videos specified in videoIds list
function getVideoMetadata(videoIds){
    // https://developers.google.com/youtube/v3/docs/videos/list
    var request = gapi.client.youtube.videos.list({
      // The 'id' property's value is a comma-separated string of video IDs.
      id: videoIds.join(','),
      part: 'id,snippet,statistics,contentDetails,localizations,recordingDetails'
    });
    
    request.execute(function(response) {
        // reads the detailed metadata
//        parseResponse(response.result);
        responseData = response;
        parseResponse(responseData);
    })
    
}